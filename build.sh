#!/bin/sh

conf=0
inst=0

if uname | grep -i "MINGW32"; then
    prefix=/gnutls
else
    prefix=/opt/conferencing
fi

until [ -z "$1" ]
  do
    case $1 in
     --configure)  conf=1 ;;
     --prefix) prefix=$2 ; shift ;;
     --install) inst=1 ;;
     *) echo "Unknown option $1"; exit -1 ;;
    esac
    shift
  done  

if ! echo $PATH | grep "$prefix"; then
    echo "Adding bin directory to path"
    export PATH=$prefix/bin:$PATH
fi

if (( inst == 0 )); then
    if (( conf == 1 )); then
        echo "Configuring with prefix $prefix"
    fi

    echo "Building libapr"
    if (( conf == 1 )); then
        ./configure --enable-threads --prefix=$prefix
    fi
    make || exit -1
fi

if (( inst == 1 )); then
    echo "Installing libapr"
    make install || exit -1
fi

